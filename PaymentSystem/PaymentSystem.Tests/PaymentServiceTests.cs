﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using System.Threading.Tasks;
using NUnit.Framework;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Infrastructure.Repository;
using PaymentSystem.Infrastructure;
using MockQueryable.Moq;
using AutoMapper;
using PaymentSystem.WebApi.MappingProfile;
using PaymentSystem.Service;

namespace PaymentSystem.Tests
{
    [TestFixture]
    public class PaymentServiceTests
    {
        List<Payment> _payments;

        [SetUp]
        public void SetUp()
        {
            _payments = new List<Payment> {
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,8,21) },
                new Payment { UserGuid = new Guid("7FDAA472-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,7,21) },
                new Payment { UserGuid = new Guid("7FDAA473-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,6,21) },
                new Payment { UserGuid = new Guid("7FDAA474-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,5,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,4,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,3,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,2,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,1,21) },

            };
        }

        [Test]
        public async Task GetPayments_ExistingUserId_ReturnsPayments()
        {
            var userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");

            var mockDbContext = Mock.Of<PaymentSystemDbContext>();

            var repository = Mock.Of<IPaymentRepository>();
            Mock.Get(repository).Setup(o => o.GetPayments(userGuid, 50, 1)).Returns(Task.FromResult(_payments.AsEnumerable()));
            var configuration = new MapperConfiguration(o => o.AddProfile(new PaymentMappingProfile()));
            var mapper = configuration.CreateMapper();

            var service = new PaymentService(repository, mapper);
            var result = await service.GetPayments(userGuid,50,1);

            Assert.IsNotEmpty(result);

        }

    }
}
