﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;
using PaymentSystem.WebApi.Controllers;

namespace PaymentSystem.Tests
{
    [TestFixture]
    class PaymentControllerTests
    {
        Mock<IUserService> _mockUserService;
        Mock<IPaymentService> _mockPaymentService;
        IMapper _mockMapper;

        IEnumerable<PaymentDto> _payments;

        [SetUp]
        public void Setup()
        {
            _mockUserService = new Mock<IUserService>();
            _mockPaymentService = new Mock<IPaymentService>();
            _mockMapper = Mock.Of<IMapper>();

            _payments = new List<PaymentDto> {

                new PaymentDto {
                Amount = 2500M,
                Date = new DateTime(2019, 8, 16),
                Reason = "Payment Completed",
                Status = "Closed"
            },
                new PaymentDto
                {
                Amount = 2500M,
                Date = new DateTime(2021, 8, 16),
                Reason = string.Empty,
                Status = "Pending"
                }

            };


        }
        [Test]
        public async Task Get_UserExists_ReturnsPayments()
        {
            _mockPaymentService.Setup(n => n.GetPayments(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>()));
            _mockUserService.Setup(userService => userService.Exists(It.IsAny<Guid>())).Returns(Task<bool>.FromResult(true));
            _mockPaymentService.Setup(paymentService => paymentService.GetPayments(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>())).Returns(Task.FromResult(_payments));

            PaymentController controller = new PaymentController(_mockPaymentService.Object, _mockUserService.Object);
            var response = await controller.Get(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>());
            var result = response.Result as OkObjectResult;
            Assert.NotNull(result);
            var payments = (result.Value as List<PaymentDto>);

            Assert.NotNull(payments);
            Assert.That(() => payments.Count > 0);
        }

        [Test]
        public async Task Get_UserDontExist_ReturnsNotFound()
        {
            _mockPaymentService.Setup(n => n.GetPayments(It.IsAny<Guid>(), 50, 1));
            _mockUserService.Setup(userService => userService.Exists(It.IsAny<Guid>())).Returns(Task<bool>.FromResult(false));
            _mockPaymentService.Setup(paymentService => paymentService.GetPayments(It.IsAny<Guid>(), 50, 1)).Returns(Task.FromResult(_payments));

            PaymentController controller = new PaymentController(_mockPaymentService.Object, _mockUserService.Object);
            var response = await controller.Get(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>());

            Assert.NotNull(response);
            Assert.IsInstanceOf<NotFoundResult>(response.Result);

        }

        [Test]
        public async Task Get_UserExists_ReturnsOkObjectResult()
        {
            _mockPaymentService.Setup(n => n.GetPayments(It.IsAny<Guid>(), 50, 1));
            _mockUserService.Setup(userService => userService.Exists(It.IsAny<Guid>())).Returns(Task<bool>.FromResult(true));
            _mockPaymentService.Setup(paymentService => paymentService.GetPayments(It.IsAny<Guid>(), 50, 1)).Returns(Task.FromResult(_payments));

            PaymentController controller = new PaymentController(_mockPaymentService.Object, _mockUserService.Object);
            var response = await controller.Get(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>());

            Assert.NotNull(response);
            Assert.IsInstanceOf<OkObjectResult>(response.Result);

        }

    }
}
