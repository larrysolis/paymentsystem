﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MockQueryable.Moq;
using Moq;
using NUnit.Framework;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure;
using PaymentSystem.Infrastructure.Repository;

namespace PaymentSystem.Tests
{
    [TestFixture]
    class BalanceRepositoryTests
    {
        List<Balance> _balances;

        Guid _userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");

        [SetUp]
        public void SetUp()
        {
            _balances = new List<Balance> { new Balance { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Value = 3000, DateCreated = new DateTime(2021,8,5), Deleted = false},
            new Balance { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"),Value = 3000, DateCreated = new DateTime(2021,7,5), Deleted = false }};
        }

        [Test]
        public void GetLatest_ExistingUser_ReturnsBalance()
        {
            var mockDbSet = _balances.AsQueryable().BuildMockDbSet();
            var mockDbContext = Mock.Of<PaymentSystemDbContext>();
            Mock.Get(mockDbContext).Setup(o => o.Set<Balance>()).Returns(mockDbSet.Object);
            var mockBalRepository = new BalanceRepository(mockDbContext);

            var result = mockBalRepository.GetLatest(_userGuid);
            var actual = _balances.Where(o => o.UserGuid == _userGuid).FirstOrDefault().Value;
            
            Assert.NotNull(result);
            Assert.AreEqual(result.Value, actual);

        }

        [Test]
        public void GetLatest_ExistingUser_ReturnsLatestBalance()
        {
            var mockDbSet = _balances.AsQueryable().BuildMockDbSet();
            var mockDbContext = Mock.Of<PaymentSystemDbContext>();
            Mock.Get(mockDbContext).Setup(o => o.Set<Balance>()).Returns(mockDbSet.Object);
            var mockBalRepository = new BalanceRepository(mockDbContext);

            var result = mockBalRepository.GetLatest(_userGuid);
            var actual = _balances.Where(o => o.UserGuid == _userGuid).OrderByDescending(o => o.DateCreated).FirstOrDefault().Value;

            Assert.NotNull(result);
            Assert.AreEqual(result.Value, actual);

        }

    }
}
