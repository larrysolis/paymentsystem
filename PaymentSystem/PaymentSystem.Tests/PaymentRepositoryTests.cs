﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MockQueryable.Moq;
using Moq;
using NUnit.Framework;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Infrastructure.Repository;
using PaymentSystem.Service;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.Tests
{
    [TestFixture]
    public class PaymentRepositoryTests
    {
        List<Payment> _payments;

        [SetUp]
        public void SetUp()
        {
           _payments = new List<Payment> {
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,8,21) },
                new Payment { UserGuid = new Guid("7FDAA472-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,7,21) },
                new Payment { UserGuid = new Guid("7FDAA473-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,6,21) },
                new Payment { UserGuid = new Guid("7FDAA474-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,5,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,4,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,3,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,2,21) },
                new Payment { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Amount = 5000, DateCreated = new DateTime(2021,1,21) },

            };
        }

        [Test]
        public async Task GetPayments_ExistingUserGuid_ReturnsPaymentsByGuId()
        {

            var userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");
            var mockDbSet = _payments.AsQueryable().BuildMockDbSet();


            //var mockPaymentDbSet = new Mock<DbSet<Payment>>();
            //mockPaymentDbSet.As<IQueryable<Payment>>().Setup(o => o.Provider).Returns(queryable.Provider);
            //mockPaymentDbSet.As<IQueryable<Payment>>().Setup(o => o.ElementType).Returns(queryable.ElementType);
            //mockPaymentDbSet.As<IQueryable<Payment>>().Setup(o => o.Expression).Returns(queryable.Expression);
            //mockPaymentDbSet.As<IQueryable<Payment>>().Setup(o => o.GetEnumerator()).Returns(queryable.GetEnumerator());

        
                var mockDbContext = Mock.Of<PaymentSystemDbContext>();
                Mock.Get(mockDbContext).Setup(context => context.Set<Payment>()).Returns(mockDbSet.Object);
           

                IPaymentRepository repository = new PaymentRepository(mockDbContext);
                var result = await repository.GetPayments(userGuid, 50, 1);
                CollectionAssert.IsNotEmpty(result);

                //Assert that the collection contains items with guid equal to the specified guid
                Assert.That(!result.Any(r => r.UserGuid != userGuid));
            
        }

        [Test]
        public async Task GetPayments_ExistingUserId_ReturnsPaymentOrderedByDesc()
        {
            var userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");
            var mockDbSet = _payments.AsQueryable().BuildMockDbSet();


            var mockDbContext = Mock.Of<PaymentSystemDbContext>();
            Mock.Get(mockDbContext).Setup(context => context.Set<Payment>()).Returns(mockDbSet.Object);

            IPaymentRepository repository = new PaymentRepository(mockDbContext);
            var result = await repository.GetPayments(userGuid, 50, 1);
            CollectionAssert.IsNotEmpty(result);

            var actual = _payments.OrderByDescending(p => p.DateCreated).Where(o => o.UserGuid == userGuid);
            CollectionAssert.AreEqual(result, actual);

        }

        [Test]
        public async Task GetPayments_ExistingUserId_ReturnCorrectNumberOfPayments()
        {
            var userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");
            var mockDbSet = _payments.AsQueryable().BuildMockDbSet();


            var mockDbContext = Mock.Of<PaymentSystemDbContext>();
            Mock.Get(mockDbContext).Setup(context => context.Set<Payment>()).Returns(mockDbSet.Object);

            IPaymentRepository repository = new PaymentRepository(mockDbContext);
            var result = await repository.GetPayments(userGuid, 2, 1);
            CollectionAssert.IsNotEmpty(result);

          
            Assert.AreEqual(result.Count(), 2);

        }
    }
}
