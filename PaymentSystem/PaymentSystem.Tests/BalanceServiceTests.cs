﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Moq;
using NUnit.Framework;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Service;
using PaymentSystem.Service.Dto;
using PaymentSystem.WebApi.MappingProfile;

namespace PaymentSystem.Tests
{
    [TestFixture]
    public class BalanceServiceTests
    {
        Balance _balance;

        Guid _userGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E");

        [SetUp]
        public void SetUp()
        {
            _balance = new Balance { UserGuid = new Guid("7FDAA471-34ED-4052-B52B-B4B658BC3A7E"), Value = 3000, DateCreated = new DateTime(2021,8,5), Deleted = false};
        }

        [Test]
        public void  GetLatestBalance_ExistingUser_ReturnsCorrectBalance()
        {
            var configuration = new MapperConfiguration(o => o.AddProfile(new BalanceMappingProfile()));
            var mapper = configuration.CreateMapper();

            var mockBalRepository = new Mock<IBalanceRepository>();
            mockBalRepository.Setup(o => o.GetLatest(_userGuid)).Returns(_balance);

            BalanceService service = new BalanceService(mockBalRepository.Object, mapper);
            var result = service.GetLatestBalance(_userGuid);
            Assert.NotNull(result);
            Assert.AreEqual(result.Value, _balance.Value);
        }
    }
}
