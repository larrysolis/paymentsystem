﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using PaymentSystem.Infrastructure.Identity;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;
using PaymentSystem.WebApi.Controllers;

namespace PaymentSystem.Tests
{
    [TestFixture]
    public class BalanceControllerTests
    {
        Mock<IUserService> mockUserService;
        Mock<IBalanceService> mockBalService;

        [SetUp]
        public void Init()
        {
            mockUserService = new Mock<IUserService>();
            mockBalService = new Mock<IBalanceService>();

        }


        [Test]
        public async Task Get_UserExists_ReturnsCorrrectBalance()
        {

            var userBalance = new BalanceDto { Balance = 5000 };
            mockUserService.Setup(userService => userService.Exists(It.IsAny<Guid>())).Returns(Task<bool>.FromResult(true));
            mockBalService.Setup(balService => balService.GetLatestBalance(It.IsAny<Guid>())).Returns(userBalance);
            var mockUsrService = new Mock<UserManager<AppUser>>();
            mockUsrService.Setup(n => n.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new AppUser()));

            BalanceController balController = new BalanceController(mockBalService.Object, mockUsrService.Object);
            var response = await balController.Get(It.IsAny<Guid>());
            var result = response.Result as OkObjectResult;

            mockUserService.Verify(usrService => usrService.Exists(It.IsAny<Guid>()));
            mockBalService.Verify(balService => balService.GetLatestBalance(It.IsAny<Guid>()));
            Assert.NotNull(result);

            Assert.AreEqual(userBalance.Balance, (result.Value as BalanceDto).Balance);
        }
        [Test]
        public async Task Get_UserExists_ReturnsOkResult()
        {

            var mockUsrService = new Mock<UserManager<AppUser>>();
            mockUsrService.Setup(n => n.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new AppUser()));

            BalanceController balController = new BalanceController(mockBalService.Object, mockUsrService.Object);
            var response = await balController.Get(It.IsAny<Guid>());

            mockUserService.Verify(usrService => usrService.Exists(It.IsAny<Guid>()));

            Assert.IsInstanceOf<OkObjectResult>(response.Result);

        }

        [Test]
        public async Task Get_UserDontExist_ReturnsNotFoundResult()
        {

            var mockUsrService = new Mock<UserManager<AppUser>>();
            mockUsrService.Setup(n => n.FindByIdAsync(It.IsAny<string>())).Returns(Task.FromResult(new AppUser()));

            BalanceController balController = new BalanceController(mockBalService.Object, mockUsrService.Object);
            var response = await balController.Get(It.IsAny<Guid>());

            mockUserService.Verify(usrService => usrService.Exists(It.IsAny<Guid>()));

            Assert.IsInstanceOf<NotFoundResult>(response.Result);

        }

    }
}
