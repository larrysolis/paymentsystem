﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;

namespace PaymentSystem.Infrastructure.Repository
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly PaymentSystemDbContext _dbContext;
        private DbSet<Payment> _payments;


        public PaymentRepository(PaymentSystemDbContext dbContext)
        {
            _dbContext = dbContext;
            _payments = _dbContext.Set<Payment>();
        }

        public async Task<IEnumerable<Payment>> GetPayments(Guid userGuid, int itemCount, int page = 1)
        {
            return await _payments.Where(payment => payment.UserGuid == userGuid)
                .OrderByDescending(payment => payment.DateCreated)
                .Skip(itemCount * (page-1))
                .Take(itemCount)
                .ToListAsync();
        }
    }
}
