﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;

namespace PaymentSystem.Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly PaymentSystemDbContext _dbContext;
        DbSet<User> _users;

        public UserRepository(PaymentSystemDbContext dbContext)
        {
            _dbContext = dbContext;
            _users = _dbContext.Set<User>();
        }

        public async Task<bool> Exists(Guid guid)
        {
           
            return await _users.AnyAsync(n => n.Guid == guid);
        }
    }
}
