﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;

namespace PaymentSystem.Infrastructure.Repository
{
    public class BalanceRepository : IBalanceRepository
    {
        private readonly PaymentSystemDbContext _paymentSystemDbContext;
        private DbSet<Balance> _balances;

        public BalanceRepository(PaymentSystemDbContext paymentSystemDbContext)
        {
            _paymentSystemDbContext = paymentSystemDbContext;
            _balances = _paymentSystemDbContext.Set<Balance>();
        }

        public Balance GetLatest(Guid userGuid)
        {
            return _balances.Where(n => n.Deleted == false)
                .OrderByDescending(o => o.DateCreated).FirstOrDefault();
        }
    }
}
