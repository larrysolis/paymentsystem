﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem.Infrastructure.Identity
{
    public static class UserRoles
    {
        public const string Admin = "Admin";
        public const string User = "User";

    }
}
