﻿using System;
using System.Threading.Tasks;

namespace PaymentSystem.Infrastructure.Contract
{
    public interface IUserRepository
    {
        Task<bool> Exists(Guid guid);
    }
}