﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaymentSystem.Core.Entities;

namespace PaymentSystem.Infrastructure.Contract
{
    public interface IPaymentRepository
    {
        Task<IEnumerable<Payment>> GetPayments(Guid userGuid, int itemCount, int page = 0);
    }
}