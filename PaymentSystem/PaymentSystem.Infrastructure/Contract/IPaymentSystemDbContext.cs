﻿using Microsoft.EntityFrameworkCore;
using PaymentSystem.Core.Entities;

namespace PaymentSystem.Infrastructure.Contract
{
    public interface IPaymentSystemDbContext
    {
        DbSet<Balance> Balances { get; set; }
        DbSet<Payment> Payments { get; set; }
        DbSet<User> Users { get; set; }
    }
}