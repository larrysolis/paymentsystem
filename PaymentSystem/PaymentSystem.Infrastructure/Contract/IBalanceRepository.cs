﻿using System;
using PaymentSystem.Core.Entities;

namespace PaymentSystem.Infrastructure.Contract
{

    public interface IBalanceRepository
    {

        Balance GetLatest(Guid userGuid);
    }
}