﻿using System.Linq;
using System.Reflection.Emit;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Infrastructure.Identity;

namespace PaymentSystem.Infrastructure
{
    public class PaymentSystemDbContext : IdentityDbContext<AppUser>, IPaymentSystemDbContext
    {
        public PaymentSystemDbContext()
        {

        }

        public PaymentSystemDbContext(DbContextOptions<PaymentSystemDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            SeedUsers(modelBuilder);
            SeedRoles(modelBuilder);
            SeedUserRoles(modelBuilder);
            SeedAccountData(modelBuilder);
            
        }

        private void SeedUsers(ModelBuilder builder)
        {
            AppUser user = new AppUser()
            {
                Id = "b74ddd14-6340-4840-95c2-db12554843e5",
                UserName = "larry",
                Email = "larry@gmail.com",
                LockoutEnabled = false,
                PhoneNumber = "1234567890",
                NormalizedEmail = "LARRY@GMAIL.COM",
                NormalizedUserName = "LARRY"

            };

            PasswordHasher<AppUser> passwordHasher = new PasswordHasher<AppUser>();
            var hashed = passwordHasher.HashPassword(user, "Admin!@#$123");
            user.PasswordHash = hashed;
            builder.Entity<AppUser>().HasData(user);
        }

        private void SeedRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "fab4fac1-c546-41de-aebc-a14da6895711", Name = "Admin", ConcurrencyStamp = "1", NormalizedName = "Admin" },
                new IdentityRole() { Id = "c7b013f0-5201-4317-abd8-c211f91b7330", Name = "HR", ConcurrencyStamp = "2", NormalizedName = "Human Resource" }
                );
        }

        private void SeedUserRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>() { RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", UserId = "b74ddd14-6340-4840-95c2-db12554843e5" }
                );
        }

        private void SeedAccountData(ModelBuilder modelBuilder)
{
            modelBuilder.Entity<User>().HasData(new User { Guid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"), CreatedBy = "dev", DateCreated = System.DateTime.Now });

            modelBuilder.Entity<Balance>().HasData(new Balance
            {
                Guid = new System.Guid("8FDAA471-34ED-4052-B52B-B4B658BC3A7E"),
                UserGuid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"),
                Value = 3000,
                CreatedBy = "dev",
                DateCreated = System.DateTime.Now
});

            modelBuilder.Entity<Payment>().HasData(
                new Payment
                {
                    Guid = new System.Guid("7FDAA472-34ED-4052-B52B-B4B658BC3A7E"),
                    UserGuid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"),
                    Amount = 500,
                    DateCreated = new System.DateTime(2021, 9, 10),
                    CreatedBy = "dev",
                    Status = Core.Enum.PaymentStatus.Complete
                },
                 new Payment
                 {
                     Guid = new System.Guid("7FDAA473-34ED-4052-B52B-B4B658BC3A7E"),
                     UserGuid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"),
                     Amount = 5000,
                     DateCreated = new System.DateTime(2021, 8, 10),
                     CreatedBy = "dev",
                     Status = Core.Enum.PaymentStatus.Complete
                 },
                 new Payment
                 {
                     Guid = new System.Guid("7FDAA474-34ED-4052-B52B-B4B658BC3A7E"),
                     UserGuid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"),
                     Amount = 200,
                     DateCreated = new System.DateTime(2021, 7, 10),
                     CreatedBy = "dev",
                     Status = Core.Enum.PaymentStatus.Complete
                 },
                 new Payment
                 {
                     Guid = new System.Guid("7FDAA475-34ED-4052-B52B-B4B658BC3A7E"),
                     UserGuid = new System.Guid("b74ddd14-6340-4840-95c2-db12554843e5"),
                     Amount = 2000,
                     DateCreated = new System.DateTime(2021, 6, 10),
                     CreatedBy = "dev",
                     Status = Core.Enum.PaymentStatus.Complete
                 }
            );
        }
    

    public DbSet<Payment> Payments { get; set; }
        public DbSet<Balance> Balances { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
