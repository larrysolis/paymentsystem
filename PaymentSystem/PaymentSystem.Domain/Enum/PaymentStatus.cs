﻿namespace PaymentSystem.Core.Enum
{
    public enum PaymentStatus
    {
        Pending = 1,
        Success = 2,
        Complete = 3,
        Closed = 4
    }
}
