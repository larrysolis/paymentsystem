﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentSystem.Core.Entities
{
    public abstract class AuditedModelBase
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Guid { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [Required]
        [MaxLength(30)]

        public string CreatedBy { get; set; }
        [MaxLength(30)]

        public string ModifiedBy { get; set; }

        public bool Deleted { get; set; }

        public DateTime DateDeleted {get;set; }

        public string DeletedBy { get; set; }
    }
}
