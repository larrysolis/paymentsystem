﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PaymentSystem.Core.Entities
{
    public class Balance : AuditedModelBase
    {
        public User User { get; set; }

        [ForeignKey("User")]
        public Guid UserGuid { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Value { get; set; }

    }
}
