﻿using System.Collections.Generic;

namespace PaymentSystem.Core.Entities
{
    public class User : AuditedModelBase
    {
        public virtual IEnumerable<Payment> Payments { get; set; }
        public virtual IEnumerable<Balance> Balances { get; set; }
    }
}
