﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PaymentSystem.Core.Enum;

namespace PaymentSystem.Core.Entities
{
    public class Payment : AuditedModelBase
    {


        [Required]
        [Column(TypeName = "decimal(18,4)")]
        public decimal Amount { get; set; }

        [Required]
        public PaymentStatus Status { get; set; }

        [MaxLength(50)]
        public string Reason { get; set; }

        [ForeignKey("User")]
        public Guid UserGuid { get; set; }

        public User User { get; set; }



    }
}
