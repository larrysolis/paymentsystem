﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using PaymentSystem.Infrastructure.Identity;

namespace PaymentSystem.WebApi.Data
{
    public class UserSeed
    {
        public static void SeedUsers(UserManager<AppUser> userManager)
        {
            if (userManager.FindByEmailAsync("larry@gmail.com").Result == null)
            {
                var user = new AppUser
                {
                    UserName = "larry",
                    Email = "larry@gmail.com"
                };

                IdentityResult result = userManager.CreateAsync(user, "Admin!@#$123").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }
        }
    }
}
