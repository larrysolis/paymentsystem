﻿using System;
using AutoMapper;
using PaymentSystem.Core.Entities;
using PaymentSystem.Core.Enum;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.WebApi.MappingProfile
{
    public class PaymentMappingProfile : Profile
    {
        public PaymentMappingProfile()
        {
            CreateMap<Payment, PaymentDto>()
                .ForMember(p => p.Status, opt => opt.MapFrom(o => Enum.GetName(typeof(PaymentStatus), o.Status)))
                .ForMember(q => q.Date, opt => opt.MapFrom(r => r.DateCreated)); 
                
        }
    }
}
