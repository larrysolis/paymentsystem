﻿using AutoMapper;
using PaymentSystem.Core.Entities;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.WebApi.MappingProfile
{
    public class BalanceMappingProfile : Profile
    {
        public BalanceMappingProfile()
        {
            CreateMap<Balance, BalanceDto>();
                
        }
    }
}
