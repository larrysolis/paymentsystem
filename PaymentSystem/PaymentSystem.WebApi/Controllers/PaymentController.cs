﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.WebApi.Controllers
{
    [ApiController]
    [Route("Account/{userGuid}/Payments")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly IUserService _userService;

        public PaymentController(IPaymentService paymentService, IUserService userService)
        {
            _paymentService = paymentService;
            _userService = userService;
        }


        [HttpGet]
        [HttpHead]
        public async Task<ActionResult<IEnumerable<PaymentDto>>> Get(Guid userGuid, int itemCount, int page)
        {

            var existingUser = await _userService.Exists(userGuid);
            if (existingUser)
            {
                return Ok(await _paymentService.GetPayments(userGuid, itemCount, page));
            }
            else
            {
                return NotFound();
            }

        }

    }
}
