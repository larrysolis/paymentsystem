﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PaymentSystem.Infrastructure.Identity;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.WebApi.Controllers
{
    [ApiController]
    [Authorize]

    [Route("Account/{userGuid}/Balance")]
    public class BalanceController : ControllerBase
    {
        private readonly IBalanceService _balanceService;
        private readonly UserManager<AppUser> _userManager;

        public BalanceController(IBalanceService balanceService, UserManager<AppUser> userManager)
        {
            _balanceService = balanceService;
            _userManager = userManager;
        }

        [HttpGet]
        [HttpHead]
        public async Task<ActionResult<BalanceDto>> Get([FromRoute] Guid userGuid)
        {
            var usr = await _userManager.FindByIdAsync(userGuid.ToString());

          
            if (usr != null)
            {
                var balance = _balanceService.GetLatestBalance(userGuid);
                return Ok(balance);
            }
            else { return NotFound(); }
        }
    }
}
