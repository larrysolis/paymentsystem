﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PaymentSystem.Infrastructure.Identity;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.WebApi.Controllers
{
    [ApiController]
    [Route("account/{userGuid}")]
    public class AccountController : ControllerBase
    {
        private readonly IBalanceService _balanceService;
        private readonly IPaymentService _paymentService;
        private readonly UserManager<AppUser> _userManager;

        public AccountController(IBalanceService balanceService, IPaymentService paymentService, UserManager<AppUser> userManager)
        {
            _balanceService = balanceService;
            _paymentService = paymentService;
            _userManager = userManager;
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<AccountDto>> Get([FromRoute] Guid userGuid, int itemCount = 10, int page = 1)
{
            var usr = await _userManager.FindByIdAsync(userGuid.ToString());
            if(usr == null) { return NotFound(); }
            var balance = _balanceService.GetLatestBalance(userGuid);
            var paymentsList = await _paymentService.GetPayments(userGuid, itemCount, page);
            var accountData = new AccountDto
            {
                Balance = balance,
                Payments = paymentsList
            };
            return Ok(accountData);
        }
    }
}
