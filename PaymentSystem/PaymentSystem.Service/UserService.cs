﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using PaymentSystem.Infrastructure.Contract;

namespace PaymentSystem.Service.Contract
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<bool> Exists(Guid guid)
        {
            return await _userRepository.Exists(guid);
        }
    }
}
