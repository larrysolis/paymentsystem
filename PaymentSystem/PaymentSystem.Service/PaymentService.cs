﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.Service
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IMapper _mapper;

        public PaymentService(IPaymentRepository paymentRepository, IMapper mapper)
        {
            _paymentRepository = paymentRepository;
            _mapper = mapper;
        }
        /// <summary>
        /// Returns a list of user's payments
        /// </summary>
        /// <param name="userGuid">the user's guid</param>
        /// <param name="itemCount">the count of item to be displayed per page</param>
        /// <param name="page">page number</param>
        /// <returns>List of payments</returns>
        public async Task<IEnumerable<PaymentDto>> GetPayments(Guid userGuid, int itemCount, int page)
        {
            var payments = await _paymentRepository.GetPayments(userGuid, itemCount, page);
            return _mapper.Map<IEnumerable<Payment>, IEnumerable<PaymentDto>>(payments);
        }

    }
}
