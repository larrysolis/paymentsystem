﻿using System;

namespace PaymentSystem.Service.Dto
{
    public class PaymentDto
    {
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
        public DateTime Date { get; set; }

    }
}
