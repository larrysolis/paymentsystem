﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentSystem.Service.Dto
{
    public class AccountDto
    {
        public BalanceDto Balance { get; set; }

        public IEnumerable<PaymentDto> Payments { get; set;}
    }
}
