﻿namespace PaymentSystem.Service.Dto
{
    public class BalanceDto
    {
         public decimal Value { get; set; }
    }
}
