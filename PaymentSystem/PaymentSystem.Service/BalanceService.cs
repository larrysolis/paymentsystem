﻿using System;
using AutoMapper;
using PaymentSystem.Core.Entities;
using PaymentSystem.Infrastructure.Contract;
using PaymentSystem.Service.Contract;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.Service
{
    public class BalanceService : IBalanceService
    {
        private readonly IBalanceRepository _balanceRepository;
        private readonly IMapper _mapper;

        public BalanceService(IBalanceRepository balanceRepository, IMapper mapper)
        {
            _balanceRepository = balanceRepository;
            _mapper = mapper;
        }

        public BalanceDto GetLatestBalance(Guid userGuid)
        {
            return _mapper.Map<Balance, BalanceDto>(_balanceRepository.GetLatest(userGuid));
        }
    }
}
