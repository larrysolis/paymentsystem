﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.Service.Contract
{
    public interface IPaymentService
    {
        Task<IEnumerable<PaymentDto>> GetPayments(Guid userGuid, int itemCount, int page);
    }
}