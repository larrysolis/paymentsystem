﻿using System;
using System.Threading.Tasks;

namespace PaymentSystem.Service.Contract
{
    public interface IUserService
    {
        Task<bool> Exists(Guid guid);
    }
}