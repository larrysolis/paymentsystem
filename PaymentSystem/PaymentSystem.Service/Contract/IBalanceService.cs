﻿using System;
using PaymentSystem.Service.Dto;

namespace PaymentSystem.Service.Contract
{
    public interface IBalanceService
    {
        BalanceDto GetLatestBalance(Guid userGuid);
    }
}