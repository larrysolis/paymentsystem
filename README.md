
## User Account API

an api for viewing a user's balance and list of payments ordered by date

### How to get started
* open PaymentSystem.sln
* open appsettings.json in PaymentSystem.WebApi project, and replace the connection string.
* open package manager console, set 'PaymentSystem.Infrastructure' as the default project
* type update-database and press enter.
* Set 'PaymentSystem.WebApi' us Startup Project and press run

* To view list of payments and account balance,  create a request using postman using the following url:

` http://localhost:63765/account/b74ddd14-6340-4840-95c2-db12554843e5 `

* In the Headers tab, add a key named 'Authorization' and Value  

`Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoibGFycnkiLCJqdGkiOiJhMjNjZmJkNC1lMjg0LTRiM2EtOTE3Ny02Y2QyOTI3NjcwZGYiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJBZG1pbiIsImV4cCI6MTYzMjEyNjc0OCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo2Mzc2NSIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6NDIwMCJ9.bCsDEnX6IvfU_H4IKujCKebNTppdFr5XCgIXYm7paPU`



you can view the Swagger UI for more details in 
`http://localhost:63765/swagger/index.html`

# Requirements
This solution requires .Net Core 3.1

# Structure
* PaymentSystem.Core - Contains the Domain 


* PaymentSystem.Infrastructure - holds the data access layer


* PaymentSystem.Service - holds interfaces which are used to communicate between the UI layer and repository layer


* PaymentSystem.WebApi - holds the web api

* PaymentSystem.Tests - holds the unit tests